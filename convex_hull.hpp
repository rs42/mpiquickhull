#pragma once
#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <functional> //for std::hash
#include <queue>
#include <unordered_set>
#include <fstream>
#include <mpi.h>
#include "geometry.hpp"

#include <stdint.h>
#include <limits.h>
//https://stackoverflow.com/questions/40807833/sending-size-t-type-data-with-mpi
#if SIZE_MAX == UCHAR_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#else
   #error "what is happening here?"
#endif


struct Convex_hull
{
  const int dim;
  //there are dim+1 facets and (dim+1)*dim/2 ridges in dim-simplex
  const int num_simplex_facets;
  const int num_simplex_ridges;
  const int num_ridge_vertices;
  const int num_facet_incident_ridges;
  const int num_facet_vertices;
private:
  
  void create_initial_simplex();
  void assign_to_outside_sets(std::list<Facet> & fl, std::vector<int> & p);
  void assign_to_outside_sets(std::list<Facet> & fl, double * pp, const int ppsz, const int st = 0);//ppsz is size in points!

  void build_cone(const int apex, std::vector<std::list<Ridge>::iterator> & horizon_ridges, std::list<Facet> & new_facets, std::list<Ridge> & new_ridges);

  bool find_facet_with_apex_easy(std::list<Facet>::iterator & ff);
  bool find_facet_with_apex_max(std::list<Facet>::iterator & ff);
  bool os_points_here;
  std::unordered_set<std::list<Facet>::iterator, Facet_it_hash, Facet_KeyEqual> facet_hash_set;
public:
  std::list<Facet> facet_list;
  std::list<Ridge> ridge_list;

  double talking_time;
  double searches_time;
  double big_time;
  double cone_time;
  double* points; //set of points, in the beginning it's just scattered points, then apex points will be added
  int points_sz_points;
  int points_sz_doubles;
  int points_cur_points;

  double* central_point; //for plane orientation

  double* mpi_points;//used only by #0 node to store all initial points
  int mpi_points_sz_points;
  int mpi_points_sz_doubles;

  MPI_Comm my_comm;
  int mpirank, mpinum, cur_sender;
  int cur_sender_it, valid_size;
  std::vector<int> senders;
  int numto_recv;
  //constructor for #0
  Convex_hull(int d, MPI_Comm comm, int nrec, const std::vector<Point> & p)
  : dim(d), num_simplex_facets(dim+1), num_simplex_ridges((dim+1)*dim/2),
    num_ridge_vertices(dim-1), num_facet_incident_ridges(dim),
    num_facet_vertices(dim), my_comm(comm), numto_recv(nrec)
  {
    talking_time = searches_time = 0;
    big_time = 0; 
    cone_time = 0;
    assert(dim >= 2);
    assert(p.size() >= dim + 1 && nrec >= dim + 1);
    MPI_Comm_rank(my_comm, &mpirank);
    MPI_Comm_size(my_comm, &mpinum);
    cur_sender = 0;
    central_point = new double [dim];
    os_points_here = 1;
    mpi_points_sz_points = p.size();
    mpi_points_sz_doubles = dim*mpi_points_sz_points;
    mpi_points = new double [mpi_points_sz_doubles];
    //convert p into mpi_points
    for (int i = 0; i < mpi_points_sz_points; i++) {
      for (int j = 0; j < dim; j++)
        mpi_points[dim*i+j] = p[i][j];
    }
    
    assert(numto_recv*mpinum == mpi_points_sz_points);
    
    for (int i = 0; i < mpinum; i++)
      senders.push_back(i);
    cur_sender_it = 0;
    valid_size = mpinum;
    points_cur_points = numto_recv;
    points_sz_points = mpi_points_sz_points+dim+2;
    points_sz_doubles = points_sz_points*dim;
    points = new double [points_sz_doubles];
    facet_hash_set.rehash(points_sz_points*dim*(dim-1)/2); //maybe bigger??
  }
  //constructor for everyone else
  Convex_hull(int d, MPI_Comm comm, int nrec)
  : dim(d), num_simplex_facets(dim+1), num_simplex_ridges((dim+1)*dim/2),
    num_ridge_vertices(dim-1), num_facet_incident_ridges(dim),
    num_facet_vertices(dim), my_comm(comm), numto_recv(nrec)
  {

    talking_time = searches_time = 0;
    big_time = 0;
    cone_time = 0;
    cur_sender = 0;
    assert(dim >= 2);
    assert(nrec >= dim + 1);
    MPI_Comm_rank(my_comm, &mpirank);
    MPI_Comm_size(my_comm, &mpinum);
    for (int i = 0; i < mpinum; i++)
      senders.push_back(i);
    cur_sender_it = 0;
    valid_size = mpinum;
    central_point = new double [dim];
    os_points_here = 1;
    mpi_points = 0;
    points_cur_points = numto_recv;
    points_sz_points = numto_recv*mpinum+dim+2;
    points_sz_doubles = dim*points_sz_points;
    points = new double [points_sz_doubles];

    facet_hash_set.rehash(2*points_sz_points*(dim-1));
  }

  ~Convex_hull()
  {
    delete [] points;
    delete [] central_point;
    delete [] mpi_points;
  }

  void export_gnuplot_data(const char * points_filename, const char * segments_filename);
  void check();
  void find();
  void print_info() const
  {
    int as = 0;
    std::cout << "#facets: " << facet_list.size() << std::endl;
    std::cout << "#ridges: " << ridge_list.size() << std::endl;
    for (auto &i : facet_list)
      std::cout << "F" << as++ << "  : "  << i << std::endl;
  }

  void distribute(); //call this function to send data from mpi_points
 
  void export_gnuplot_data_mpi(const char * points_filename, const char * segments_filename);
  void print_num_of_facets_and_points();
  void export_points_qhull(const char * points_filename); 
 
};

void Convex_hull::export_points_qhull(const char * points_filename)
{
  //if (mpirank != 0) return;
  std::ofstream points_dat(points_filename);
  points_dat << std::setprecision(17);
  points_dat << dim << std::endl;
  points_dat << mpi_points_sz_points << std::endl;
  for (int i = 0; i < mpi_points_sz_points; i++) {
    for (int j = 0; j < dim; j++)
      points_dat << mpi_points[dim*i+j] << " ";
    points_dat << std::endl;
  }
  points_dat.close();
}


void Convex_hull::print_num_of_facets_and_points()
{
  std::cout << "Facets: " << facet_list.size() << std::endl;
  //extract vertices
  std::set<int> ps;
  for (auto &r: ridge_list) {
    ps.insert(r.vertices.begin(), r.vertices.end());
  }
  std::cout << "Vertices: " <<  ps.size() << std::endl;
}

void Convex_hull::distribute()
{//receive your part of mpi_points and form points
  if (mpirank == 0) {
    MPI_Scatter(mpi_points, numto_recv*dim, MPI_DOUBLE, points, numto_recv*dim, MPI_DOUBLE, 0, my_comm);
  } else {
    MPI_Scatter(0, numto_recv*dim, MPI_DOUBLE, points, numto_recv*dim, MPI_DOUBLE, 0, my_comm);
  }
}

void Convex_hull::export_gnuplot_data(const char * points_filename, const char * segments_filename)
{
  std::string points_filename_s(points_filename), segments_filename_s(segments_filename);
  points_filename_s += std::to_string(mpirank);
  segments_filename_s += std::to_string(mpirank);
  if (dim == 2) {
    std::ofstream points_dat(points_filename_s);
    for (int i = 0; i < points_cur_points; i++) {
      points_dat << points[dim*i] << " " << points[dim*i+1] << std::endl;
    }
    
    points_dat.close();
    std::ofstream segments_dat(segments_filename_s);
    for (auto &seg: facet_list) {
      int a = seg.ridges[0]->vertices[0];
      int b = seg.ridges[1]->vertices[0];
      assert(a != b);
      segments_dat << points[a] << " " << points[a+1] << " " << points[b] << " " << points[b+1] << std::endl;
    }
    segments_dat.close();
  } else if (dim == 3) {
    std::ofstream points_dat(points_filename_s);
    for (int i = 0; i < points_cur_points; i++) {
      points_dat << points[dim*i] << " " << points[dim*i+1] << " " << points[dim*i+2] << std::endl;
    }
    points_dat.close();
    std::ofstream segments_dat(segments_filename_s);
    for (auto &seg: ridge_list) {
      int a = seg.vertices[0];
      int b = seg.vertices[1];
      assert(a != b);
      seg.check_valid();
      segments_dat << points[a] << " " << points[a+1] << " " << points[a+2] <<
          " " << points[b] << " " << points[b+1] << " " << points[b+2] << std::endl;
    }
    segments_dat.close();
  } else {
    std::cout << "no gnuplot data created: dim > 3" << std::endl;
  }
}


void Convex_hull::export_gnuplot_data_mpi(const char * points_filename, const char * segments_filename)
{
  if (mpirank != 0) return;
  if (dim == 2) {
    std::ofstream points_dat(points_filename);
    for (int i = 0; i < mpi_points_sz_points; i++) {
      points_dat << mpi_points[dim*i] << " " << mpi_points[dim*i+1] << std::endl;
    }
    points_dat.close();
    std::ofstream segments_dat(segments_filename);
    for (auto &seg: facet_list) {
      int a = seg.ridges[0]->vertices[0];
      int b = seg.ridges[1]->vertices[0];
      assert(a != b);
      segments_dat << points[a] << " " << points[a+1] << " " << points[b] <<
                   " " << points[b+1] << std::endl;
    }
    segments_dat.close();
  } else if (dim == 3) {
    std::ofstream points_dat(points_filename);
    for (int i = 0; i < mpi_points_sz_points; i++) {
      points_dat << mpi_points[dim*i] << " " << mpi_points[dim*i+1] << " "
                 << mpi_points[dim*i+2] << std::endl;
    }
    points_dat.close();
    std::ofstream segments_dat(segments_filename);
    for (auto &seg: ridge_list) {
      int a = seg.vertices[0];
      int b = seg.vertices[1];
      assert(a != b);
      seg.check_valid();
      segments_dat << points[a] << " " << points[a+1] << " " << points[a+2]
                   << " " << points[b] << " " << points[b+1] << " " << points[b+2]
                   << std::endl;
    }
    segments_dat.close();
  } else {
    std::cout << "no gnuplot data created: dim > 3" << std::endl;
  }
}

void Convex_hull::check()
{
#ifdef G_WITH_ADD_CHECKS_G
  for (auto &f:facet_list)
    f.check_valid();
  
  for (auto &r: ridge_list) {
    r.check_valid();
  }
#endif
}

bool Convex_hull::find_facet_with_apex_easy(std::list<Facet>::iterator & ff)
{
//  double t = MPI_Wtime();
  const int buf_sz = 2*dim+2;
  for (; valid_size; senders[cur_sender_it] = senders[valid_size-1], valid_size--) {
  
  cur_sender_it = (cur_sender_it+1)%valid_size;
  cur_sender = senders[cur_sender_it];

  if (mpirank == cur_sender) {
    double buf[buf_sz];//vertex and normal_s and flag
    ff = facet_list.begin();
    if (ff->apex_dist < 0) {
      buf[buf_sz-1] = -1;
      double t = MPI_Wtime();
      MPI_Bcast(buf, buf_sz, MPI_DOUBLE, cur_sender, my_comm);
      talking_time += MPI_Wtime() - t;
      os_points_here = 0;
    } else {
      for (int i = 0; i < dim; i++)
        buf[i] = points[ff->apex+i];
      for (int i = 0; i < dim+1; i++)
        buf[dim+i] = ff->normal_s[i]; 
      buf[buf_sz-1] = 1;
      double t = MPI_Wtime();
      MPI_Bcast(buf, buf_sz, MPI_DOUBLE, cur_sender, my_comm);
      talking_time += MPI_Wtime() - t;
      return 1;
    }
  } else {
    double *buf = points + dim*points_cur_points;
    double t = MPI_Wtime();
    MPI_Bcast(buf, buf_sz, MPI_DOUBLE, cur_sender, my_comm);
    talking_time+= MPI_Wtime() - t;
    if (buf[buf_sz-1] < 0)
      continue;//skip this message

    size_t facet_hash = Point_hash(dim+1, buf+dim);
    std::list<Facet> strange_list;
    strange_list.emplace_back(dim, buf+dim, facet_hash); //after all set normal_s = 0
    const std::list<Facet>::iterator strange_f = strange_list.begin();
    
    auto res = facet_hash_set.find(strange_f);
    assert(res != facet_hash_set.end());
    ff = *res;
    ff->set_apex(points_cur_points*dim);
    points_cur_points++;
    strange_f->normal_s = 0;
//    talking_time += MPI_Wtime() - t;
    return 1;
  }
    //remove cur_sender_it
  }
  //no apex points at all
  //talking_time += MPI_Wtime() - t;
  return 0;
}

/*SLOWWWWWWWWWWWW
bool Convex_hull::find_facet_with_apex_easy(std::list<Facet>::iterator & ff)
{

  double t = MPI_Wtime();

  double distance_buf[mpinum];
  if (os_points_here) {
    ff = facet_list.begin();
    distance_buf[mpirank] = ff->apex_dist; 
    if (distance_buf[mpirank] < 0)
      os_points_here = 0;
  } else {
    distance_buf[mpirank] = -1;
  }

  MPI_Allgather(MPI_IN_PLACE, 1, MPI_DOUBLE, distance_buf, 1, MPI_DOUBLE, my_comm);
 //make just ordered sends!! greatly reduce time for alot of processes

  int sel_rank;
  for (sel_rank = 0; sel_rank < mpinum; sel_rank++) {
    if (distance_buf[sel_rank] > G_EPS_G) {
      break;
    }
  }

  if (sel_rank == mpinum) {
    //no apex point found
    return 0;
  }
  
  //we selected sel_rank process as the owner of point, now it should bcast
  //index of facet and point data
  if (mpirank == sel_rank) {
    //we already selected ff as needed facet
    MPI_Bcast(points+ff->apex, dim, MPI_DOUBLE, sel_rank, my_comm);
    MPI_Bcast(&(ff->facet_hash), 1, my_MPI_SIZE_T, sel_rank, my_comm);
  } else {
    //maybe make one Bcast with new type data??????
//    size_t facet_hash;
    std::list<Facet> strange_list;
    strange_list.emplace_back(dim);
    const std::list<Facet>::iterator strange_f = strange_list.begin();
  //  strange_f->facet_hash = facet_hash;

    MPI_Bcast(points+dim*points_cur_points, dim, MPI_DOUBLE, sel_rank, my_comm);
    MPI_Bcast(&(strange_f->facet_hash), 1, my_MPI_SIZE_T, sel_rank, my_comm);
  
    int bucket_index = facet_hash_set.bucket(strange_f);
//     int miss_count = 0;

    for (auto ddd = facet_hash_set.begin(bucket_index); ddd != facet_hash_set.end(bucket_index); ddd++) {
      ff = *ddd;
      if (ff->value_at(points+dim*points_cur_points) == distance_buf[sel_rank])
        break;
//      else miss_count++;
       // std::cout << facet_hash_set.bucket_size(bucket_index) << std::endl;
    }
  //  if (miss_count > 1) std::cout << miss_count << std::endl;

    ff->set_apex(points_cur_points*dim);
    points_cur_points++;

  }

  talking_time += MPI_Wtime() - t;

  return 1;
}*/

void Convex_hull::build_cone(const int apex,
                  std::vector<std::list<Ridge>::iterator> & horizon_ridges,
                  std::list<Facet> & new_facets, std::list<Ridge> & new_ridges)
{

//for mpi you should control access to apex!!!
  assert(horizon_ridges.size() > 0);
  //each ridge and apex create new facet
  std::vector<int> new_vertices(num_ridge_vertices); //this is for ridge constructor
  //new_vertices[0] = apex;
  size_t apex_hash = Point_hash(apex);
  std::vector<size_t> new_vertices_hash(num_ridge_vertices);
  //new_vertices_hash[0] = apex_hash;
  std::unordered_set<std::list<Ridge>::iterator, Ridge_it_hash> hashed_set;
  hashed_set.rehash(2*horizon_ridges.size()*(dim-1));
//  assert(hashed_set.max_bucket_count() >= horizon_ridges.size()*(dim-1));

  std::list<Ridge> strange_list; //we need it for iterator
  strange_list.emplace_back(dim);//this ridge is only for interaction with unordered_set
                //because you can't interact only with hash without a key
  const std::list<Ridge>::iterator strange_r = strange_list.begin();


  for (auto &r: horizon_ridges) {
    //here we create new facet = r+apex


    new_facets.emplace_back(dim);
    auto f = std::prev(new_facets.end());


    r->add_another_facet1(f);
    f->ridges[0] = r;
    r->vertices.push_back(apex); //temp add apex point
    

f->find_plane(r->vertices, central_point, points);

//double ct = MPI_Wtime();
    facet_hash_set.insert(f);

//cone_time += MPI_Wtime() - ct;
    r->vertices.pop_back(); //now it can be removed

#ifdef G_WITH_ADD_CHECKS_G
    r->check_valid();
#endif
    //plus create its additional incident ridges
    int ridge_it = 1;
   
//    size_t ridge_prehash = r->hash ^ apex_hash;
    size_t ridge_prehash = r->hash;

    for (int k = 0; k < num_ridge_vertices; k++) {
      new_vertices[k] = r->vertices[k];
      new_vertices_hash[k] = r->vertices_hashes[k];
    }

    for (int i = 0; i < num_ridge_vertices; i++, ridge_it++) {
      //create new incident ridge or find it if it exists
      //apex point is in its vertices
      //new_vertices[0] = apex;

      strange_r->hash = ridge_prehash ^ r->vertices_hashes[i];
      new_vertices[i] = apex;
      new_vertices_hash[i] = apex_hash;
      //don't forget to restore it later
      
      //use hash here
      //now we know which vertices are in this ridge
      //anyway this ridge could be built for another facet earlier
      //so let's check it
      std::list<Ridge>::iterator er; //existing ridge
      bool is_copy = 0;
      
      //
      //if (hashed_set.bucket_count() > 0) {//next ops are undefined is == 0
      //
      int bucket_index = hashed_set.bucket(strange_r);
      for (auto ddd = hashed_set.begin(bucket_index); ddd != hashed_set.end(bucket_index); ddd++) {
        std::list<Ridge>::iterator dd = *ddd;
        int c, g;
        for (c = 0; c < num_ridge_vertices; c++) { //for new_vertices
          for (g = 0; g < num_ridge_vertices; g++) { //for vertices in dd
            if (dd->vertices[g] == new_vertices[c]) break;
          }
          if (g == num_ridge_vertices) break; //these ridges differ at least at one vertex
        }
        if (c == num_ridge_vertices) { //all vertices are same for ridges er and new_vertices
          is_copy = 1;
          hashed_set.erase(dd);
          er = dd;
          break;
        }
      }

      if (is_copy) {

        //use existing ridge er
        er->hash ^= apex_hash;
        er->add_another_facet2(f);
        f->ridges[ridge_it] = er;
#ifdef G_WITH_ADD_CHECKS_G
        er->check_valid();
#endif

      } else {

        //create new ridge
        new_ridges.emplace_back(dim, new_vertices, f, strange_r->hash, new_vertices_hash);

        hashed_set.insert(std::prev(new_ridges.end()));

        f->ridges[ridge_it] =  std::prev(new_ridges.end());
        //std::cout << hashed_set.load_factor() << std::endl;

      }

      //restore new_vertices and hashes
      new_vertices[i] = r->vertices[i];
      new_vertices_hash[i] = r->vertices_hashes[i];

    }

    assert(ridge_it == num_facet_incident_ridges); 
  }
}


void Convex_hull::find()
{
  create_initial_simplex();
  
  double t;
assign_to_outside_sets(facet_list, points, points_cur_points);
  
  std::list<Facet>::iterator facet_with_apex;
  while (find_facet_with_apex_easy(facet_with_apex)) {

  //  std::cout <<  "proc: " << mpirank << " apex: " << facet_with_apex->apex << " " << facet_with_apex->apex_dist << std::endl;
    //cycle until convex hull isn't ready
    //we should find the furthest point - apex - from all max_p's (_max)
    //another way (_easy) is to select random max_p as apex
    //we can do it because anyway all max_p's will be vertices of final convex hull
    //but the second way will be slower in general case
    //you must test it!results: on high dims _easy is faster! too many facets are created to find max
    //if there is no apex then convex hull is ready
    //now we should create visible set for apex
    std::vector< std::list<Facet>::iterator > facets_to_delete;
    std::vector< std::list<Ridge>::iterator > ridges_to_delete;
    std::vector< std::list<Ridge>::iterator > horizon_ridges;
    std::list<Facet> new_facets;
    std::list<Ridge> new_ridges;
    std::queue< std::list<Facet>::iterator > facets_to_visit;
    facets_to_visit.push(facet_with_apex);
    facet_with_apex->state = visible;
    auto & ftv = facets_to_visit;
    while (!ftv.empty()) {
      auto & cur_facet_it = ftv.front();
      for (auto & incident_ridge_it : cur_facet_it->ridges) {
        if (incident_ridge_it->is_broken()) continue;
        auto neighbor_facet_it = incident_ridge_it->get_neighbor_of(cur_facet_it);
        if (neighbor_facet_it->state == visible) {
          ridges_to_delete.push_back(incident_ridge_it);
          //break link between you and this neighbor at all
          incident_ridge_it->break_link();
          continue;
        }//that's not possible at all now}
        if (neighbor_facet_it->state == not_visited) {
          //it can be visible or invisible but not checked yet
          //figure it out
          if (neighbor_facet_it->is_visible(points+facet_with_apex->apex)) {
            neighbor_facet_it->state = visible;
            
            //add to queue
            facets_to_visit.push(neighbor_facet_it);
            //add incident ridge to delete list
            ridges_to_delete.push_back(incident_ridge_it);
            //break link between you and this neighbor at all
            incident_ridge_it->break_link();
            continue;
          } else {
            //mark it as invisible
            neighbor_facet_it->state = invisible;
          }
        }
        //only if neighbor_facet is invisible you can get here
        //add ridge to boundary
        horizon_ridges.push_back(incident_ridge_it);
        incident_ridge_it->remove_facet(cur_facet_it);
      }
      facets_to_delete.push_back(cur_facet_it);
      ftv.pop();
    }

    //make a cone with apex and horizon ridges
    //don't forget to change invisible state to not_visited
    for (auto &hr : horizon_ridges)
      hr->from_invisible_to_not_visited();

double b = MPI_Wtime();
    build_cone(facet_with_apex->apex, horizon_ridges, new_facets, new_ridges);

big_time+=MPI_Wtime() - b;
    //now initialize outside sets for just created facets of cone
    //from outside sets of facets to delete
    //and delete useless facets and ridges
    
   for (auto &r : ridges_to_delete)
      ridge_list.erase(r);
    ridge_list.splice(ridge_list.end(), new_ridges);

    if (os_points_here) {
      for (auto &f : facets_to_delete) {
t = MPI_Wtime();
        assign_to_outside_sets(new_facets, f->outside_points);
searches_time+=MPI_Wtime() - t;

        facet_hash_set.erase(f);

        facet_list.erase(f);
      }

     while (!new_facets.empty()) {
        if (new_facets.front().apex_dist > 0)
          facet_list.splice(facet_list.begin(), new_facets, new_facets.begin());
        else
          facet_list.splice(facet_list.end(), new_facets, new_facets.begin());
      }

    } else {
      for (auto &f : facets_to_delete) {
   
     facet_hash_set.erase(f);

        facet_list.erase(f);
      }
      facet_list.splice(facet_list.end(), new_facets);
    }

  }
}


void Convex_hull::assign_to_outside_sets(std::list<Facet> & fl, double *pp, const int ppsz, const int st)
{
  const int start = st*dim;
  const int m = start + ppsz*dim;
  for (int i = start; i < m; i+=dim) {
    for (auto &f : fl) { //maybe random searching is more effective? or just shuffle list time after time?
      if (f.is_outside(i, pp+i))
        break;
    }
  }
  fl.sort();
}


void Convex_hull::assign_to_outside_sets(std::list<Facet> & fl, std::vector<int> & p)
{
  for (auto &x : p) {
    for (auto &f : fl) {//maybe random searching is more effective? or just shuffle list time after time?
      if (f.is_outside(x, points+x))
        break;
    }
  }
}
/*UNCOMMENT ME PLZ
void Convex_hull::assign_to_outside_sets(std::list<Facet> & fl, double *pp, const int ppsz, const int st)
{//fl is the set of facets from which we select inital base facet
  //std::vector< std::list< Facet >::iterator > base_facets_trace;
  //double dr[dim+1];//dir[0] is for base facet
  std::list<Facet>::iterator base_facet;
  std::list<Facet>::iterator i_facet, max_facet;
  double max_r, i_r;
  
  const int start = st*dim;
  const int m = start + ppsz*dim;

  base_facet = max_facet = fl.begin();
  for (int i = start; i < m; i+=dim) {
    //select base_facet as the first or random element from fl
    //out point is pp+i

    max_r = base_facet->value_at_special(pp+i);
    if (max_r > G_EPS_G) {
      max_facet->is_outside_ready(i, max_r);
      continue;
    }
    while (1) {
      for (auto & incident_ridge_it : base_facet->ridges) { //iter over neighbors
        i_facet = incident_ridge_it->get_neighbor_of(base_facet);
        i_r = i_facet->value_at_special(pp+i);
        if (i_r > max_r) {
          max_r = i_r;
          max_facet = i_facet;
        }
      }
      if (max_r > G_EPS_G) {
        max_facet->is_outside_ready(i, max_r);
        base_facet = max_facet;
        break;
      } else if (max_facet != base_facet) {
        base_facet = max_facet;
      } else {
        break;
      }
    }
    //now clear all from base_facets_trace if you use it
  }
}
*/
void Convex_hull::create_initial_simplex()
{
  std::vector<int> v(dim+1); //simplex vertices, the last one is extra

  if (mpirank == 0) {//root searches in his vertices
    //at this step we must choose DIM+1 vertices for initial simplex
    //we will build it like building cone on one facet
    double v_points_mpi[(dim+1)*dim];

  /*
    //now stupid selection
    for (int i = 0; i < num_facet_vertices; i++) {
      v.push_back(points.back());
      points.pop_back();
    }
    extra = points.back();
    points.pop_back();
   //end of stupid creation
  */
  
  //select points with max and min coordinates as vertices for initial simplex
    std::vector<int> max_ind(dim, 0), min_ind(dim, 0);
  
    double max_coords[dim], min_coords[dim];
    for (int i = 0; i < dim; i++)
      max_coords[i] = min_coords[i] = points[i];// mpi_points[i];//select first point

    for (int i = dim ; i < points_cur_points*dim; i+=dim) {//mpi_points_sz_doubles; i+=dim) {
      for (int j = 0; j < dim; j++) {//over j-index of point
        if (max_coords[j] < points[i+j]) {//mpi_points[i+j]) {
          max_coords[j] = points[i+j];//mpi_points[i+j];
          max_ind[j] = i;
        } else if (min_coords[j] > points[i+j]) { //mpi_points[i+j]) {
          min_coords[j] = points[i+j];//mpi_points[i+j];
          min_ind[j] = i;
        }
      }
    }
    std::set<int> pret_ind(max_ind.begin(), max_ind.end());
    pret_ind.insert(min_ind.begin(), min_ind.end());

    int ii = 0;
    for (auto &c: pret_ind) {
      if (ii >= dim+1) break;
      v[ii++] = c;
    }
  
    int diff = dim + 1 - ii; //how many vertices we still don't have
    int i_i = 0;
    for (; diff > 0 ; diff--) {//we should add more points
      for (; i_i < points_sz_points*dim; i_i+=dim) {//mpi_points_sz_doubles; i_i+=dim) {
        if (pret_ind.count(i_i) == 0) {
          v[ii++] = i_i;
          i_i+=dim;
          break;
        }
      }
    }
    assert(ii == dim+1);
  
    
    //now share vertices with all another processes
    for (int i = 0; i < dim+1; i++) {
      for (int j = 0; j < dim; j++)
        v_points_mpi[dim*i+j] = points[v[i]+j];
       // points[dim*(i+points_cur_points)+j] = mpi_points[v[i]+j];
    }
    MPI_Bcast(v_points_mpi, dim*(dim+1), MPI_DOUBLE, 0, my_comm);
    //MPI_Bcast(points+dim*points_cur_points, dim*(dim+1), MPI_DOUBLE, 0, my_comm);
  } else {
    //receive vertices and put then in the end of points
    //and create right order!!!
    MPI_Bcast(points + points_cur_points*dim, dim*(dim+1), MPI_DOUBLE, 0, my_comm);
    for (int i = 0; i < dim+1; i++)
      v[i] = (points_cur_points+i)*dim;
    points_cur_points += (dim+1);
  }
  //now steps are the same for all processes

  //find central_point used to build proper normals for planes
  for (int i = 0; i < dim; i++) {
    central_point[i] = 0;
    for (int j = 0; j < dim+1; j++)
      central_point[i] += points[v[j]+i];
    central_point[i]/=(dim+1);
  }

  std::vector<size_t> hashes_v(dim);
//find hashes of points in v, for apex should be found in build_cone
  for (int i = 0; i < dim; i++) {
    hashes_v[i] = Point_hash(v[i]);
  }
  int apex = v[dim];//simply the last one will be apex
  v.pop_back();
 
  //build first facet
  facet_list.emplace_back(dim);
  auto f = facet_list.begin();
  std::vector<std::list<Ridge>::iterator> cone_ridges(num_facet_incident_ridges);
  //ridge boundary for build_cone
  
  for (int i = 0; i < num_facet_incident_ridges; i++) {
    ridge_list.emplace_back(dim, f);
    auto x = cone_ridges[i] = f->ridges[i] = std::prev(ridge_list.end());
    for (int k = 0, g = 0; k < num_facet_vertices; k++) {
      if (k == i) continue;
      x->vertices[g] = v[k];
      x->vertices_hashes[g++] = hashes_v[k];
    }
    x->calculate_ridge_hash();
  }

  f->find_plane(v, central_point, points);
  facet_hash_set.insert(f);
//now build cone on this facet
  std::list<Facet> new_facets;
  std::list<Ridge> new_ridges;
 build_cone(apex, cone_ridges, new_facets, new_ridges);
  facet_list.splice(facet_list.end(), new_facets);
  ridge_list.splice(ridge_list.end(), new_ridges);
  assert(facet_list.size() == num_simplex_facets);
  assert(ridge_list.size() == num_simplex_ridges); 
#ifdef G_WITH_ADD_CHECKS_G
  for (auto &i:facet_list) {
    if (i.ridges.size() != num_facet_incident_ridges)
      std::cout << "TOO MUCH ridges in one facet" << std::endl;
  }
#endif
}
