#include <iostream>
#include <vector>
#include <CGAL/point_generators_d.h>
#include <fstream>
#define EN_MPI_EN
//#undef NDEBUG
//#include <assert.h>
//#define G_WITH_ADD_CHECKS_G
#include "convex_hull.hpp"

int main(int argc, char **argv)
{
  int rank, num;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num);

  //assert(0);
  std::vector<int> dimensions = {2, 3, 4, 5};
  std::vector<int> sizes = {4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216};
  const int iters = 10;

  
  if (rank == 0) {

  std::cout << "#proc: " << num << std::endl << std::endl;

  for (auto &dim: dimensions) {
  
    for (auto &nb_points: sizes) {
      
      std::cout << "dim: " << dim << " n: " << nb_points << std::endl;
      int numto_send_to_each_proc = nb_points/num;
      double time = 0, outside_sets_time = 0, build_cone_time = 0, talking_time = 0;

      for (int i = 0; i < iters; i++) {

      std::vector<Point> points;
      double size = 1.0;
      points.reserve(nb_points);
      CGAL::Random_points_in_ball_d<Point> gen (dim, size);
      for (int i = 0; i < nb_points; ++i) points.push_back (*gen++);
  
      Convex_hull ch(dim, MPI_COMM_WORLD, numto_send_to_each_proc, points);
      ch.distribute();
      MPI_Barrier(MPI_COMM_WORLD);
      double t = MPI_Wtime();
      ch.find();
      time += MPI_Wtime() - t;
      //ch.export_points_qhull("points.dat");
      //ch.print_num_of_facets_and_points();
      outside_sets_time += ch.searches_time;
      build_cone_time += ch.big_time;
      talking_time += ch.talking_time;

      }

      std::cout << "time: " << time/iters << std::endl;
      std::cout << "outside_sets_time: " << outside_sets_time/iters << std::endl;
      std::cout << "build_cone_time: " << build_cone_time/iters << std::endl;
      std::cout << "talking_time: " << talking_time/iters << std::endl;
    }
  }

  } else {

    for (auto &dim: dimensions) {

      for (auto &nb_points: sizes) {

        int numto_send_to_each_proc = nb_points/num;

        for (int i = 0; i < iters; i++) {

        Convex_hull ch(dim, MPI_COMM_WORLD, numto_send_to_each_proc);
        ch.distribute();
        MPI_Barrier(MPI_COMM_WORLD);
        ch.find();
        
        }
      }
    }
  }
  MPI_Finalize();
  return 0;
}
