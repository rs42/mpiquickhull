#pragma once
#include <iostream>
#include <CGAL/Cartesian_d.h>
//#include <CGAL/Epick_d.h>
#include <CGAL/Homogeneous_d.h>
#include <vector>
#include <list>
#include <functional> //for std::hash
#define G_EPS_G 1e-13

typedef CGAL::Cartesian_d<double> Kernel;
//typedef CGAL::Epic_d<DIM> Kernel; //requires Eigen
//typedef CGAL::Homogeneous_d<double> Kernel;

typedef CGAL::Point_d<Kernel> Point;
//typedef CGAL::Hyperplane_d<Kernel> Plane;
//typedef CGAL::Vector_d<Kernel> Vector;

///////////////////////////////////////////////////////////////
//POINT DESCRIPTION

inline void hash_combine_impl(size_t& h, double& v)
{ 
   size_t k = std::hash<double>()(v);
   const uint64_t m = UINT64_C(0xc6a4a7935bd1e995);
   const int r = 47;

    k *= m;
    k ^= k >> r;
    k *= m;

    h ^= k;
    h *= m;

    // Completely arbitrary number, to prevent 0's
    // from hashing to 0.
    h += 0xe6546b64;
}



inline void hash_combine(size_t& seed, double& v)
{
//  seed ^= std::hash<double>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
  seed ^= std::hash<double>()(v) + 0x9E3779B97F4A7C15 + (seed<<6) + (seed>>2);
}
/*
inline size_t Point_hash(const Point & p)
{
  size_t res = 0;
  const int dim = p.dimension();
  for (int i = 0; i < dim; i++) {
    hash_combine(res, p[i]);
  }
  return res;
}*/
inline size_t Point_hash(int dim, double * p)
{
  size_t res = 0;
  for (int i = 0; i < dim; i++) {
    hash_combine_impl(res, p[i]);
  }
  return res;
}
inline size_t Point_hash(int pos)
{
  return std::hash<int>()(pos);
}
/*
struct struct_Point_hash
{
  size_t operator() (const Point & p) const //noexcept
  {
    return Point_hash(p);
  }
};
*/
//ENDOF POINT DESCRIPTION
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
//RIDGE DESCRIPTION
struct Facet;
//represented as (dim-1)-simplex

struct Ridge
{//represented as (dim-2)-simplex
private:
  std::list<Facet>::iterator r_facets[2]; //only two facets are always adjacent with this ridge
  bool is_broken_link;
  int h0, h1; //if h0 is 1 then facets[0] is valid iterator, same for h1
public:
  
  size_t hash;//xor of vertices hashes
  const int dim;
  const int num_ridge_vertices;
  std::vector<int> vertices; //inspect if it should be stored as indices for higher dimension 
  std::vector<size_t> vertices_hashes;
  
  Ridge(int d): dim(d), num_ridge_vertices(dim - 1)
  {
    vertices.resize(num_ridge_vertices);
    vertices_hashes.resize(num_ridge_vertices);
    h0 = h1 = 0;
    is_broken_link = 1;
  }
  Ridge(int d, const std::list<Facet>::iterator & f)
  : dim(d), num_ridge_vertices(dim - 1)
  {
    vertices.resize(num_ridge_vertices);
    vertices_hashes.resize(num_ridge_vertices);
    h0 = 1;
    h1 = 0;
    r_facets[0] = f;
    is_broken_link = 1;
  }

  Ridge(int d, const std::vector<int> & v, const std::list<Facet>::iterator & f, size_t myhash, const std::vector<size_t> & vertices_hash)
  : dim(d), num_ridge_vertices(dim - 1), vertices(v), vertices_hashes(vertices_hash)
  {
    hash = myhash;
    h0 = 1;
    h1 = 0;
    r_facets[0] = f;
    is_broken_link = 1;
  }
  
  void add_another_facet1(const std::list<Facet>::iterator & f)
  {
    assert(f != r_facets[0]);
    //assert(h0 == 1 && h1 == 0);
#ifdef G_WITH_ADD_CHECKS_G
    if (!(h0 == 1 && h1 == 0)) throw 1;
#endif
    h1 = 1;
    r_facets[1] = f;
    is_broken_link = 0;
  }
  void add_another_facet2(const std::list<Facet>::iterator & f)
  {
    assert(f != r_facets[0]);
    //assert(h0 == 1 && h1 == 0);
#ifdef G_WITH_ADD_CHECKS_G
    if (!(h0 == 1 && h1 == 0)) throw 2;
#endif
    h1 = 1;
    r_facets[1] = f;
    is_broken_link = 0;
  }
  
  inline void break_link() { is_broken_link = 1; }

  void from_invisible_to_not_visited();
/*  { //this is called only for boundary ridges
    //when they have only one invisible facet left
    assert(h0 == 1 && h1 == 0);
    r_facets[0]->state = not_visited;
  }*/

  void calculate_ridge_hash()
  {
    hash = 0;
    for (auto &d : vertices_hashes)
      hash ^= d;
  }

  inline bool is_broken() const
  {
    return is_broken_link;
  }

  friend std::ostream& operator<<(std::ostream& os, const Ridge& ridge)
  {
    for (auto &i: ridge.vertices)
      os << i << std::endl;
    return os;
  }

  inline std::list<Facet>::iterator get_neighbor_of(const std::list<Facet>::iterator & that_facet_it) const
  {
    assert(h0 == 1 && h1 == 1 && is_broken_link == 0);
    assert(that_facet_it == r_facets[0] || that_facet_it == r_facets[1]);
    assert(r_facets[0] != r_facets[1]);

//   wish it was like return that_facet_it ^ r_facets[0] ^ r_facets[1];
//    if (that_facet_it == r_facets[0])
//      return r_facets[1];
//    else
//      return r_facets[0];
     return r_facets[ r_facets[0] == that_facet_it ];
  }

  inline void remove_facet(const std::list<Facet>::iterator & facet_to_remove)
  {
    assert(facet_to_remove == r_facets[0] || facet_to_remove == r_facets[1]);    
    assert(h0 == 1 && h1 == 1 && is_broken_link == 0);
    assert(r_facets[0] != r_facets[1]);
    //idea it to move another facet to 0
    h1 = 0;
    is_broken_link = 1;
//    if (facet_to_remove == r_facets[0]) {
//      r_facets[0] = r_facets[1];
//    }
    r_facets[0] = r_facets[ facet_to_remove == r_facets[0] ];
  }
  
  void check_valid();
/*  {//call it when checking final convex hull
    assert(r_facets[0] != r_facets[1]);
    assert(h0 == 1 && h1 == 1 && is_broken_link == 0);
    assert(r_facets[0]->dim == dim && r_facets[1]->dim == dim);
    //not points to the unknown memory
  }*/
};

struct Ridge_it_hash
{
  size_t operator() (const std::list<Ridge>::iterator & r) const //noexcept
  {
    return r->hash;
  }
};
//ENDOF RIDGE DESCRIPTION
////////////////////////////////////////

////////////////////////////////////////
//FACET DESCRIPTION

typedef enum {visible, not_visited, invisible} State;

struct Facet
{//represented as (dim-1)-simplex
public:

  double * normal_s;
  size_t facet_hash;

  double * p_spec;
  double p_spec_r;
  double value_at_special(double * p)
  {
    if (p != p_spec) {
      p_spec = p;
      p_spec_r = 0;
      for (int i = 0; i < dim; i++)
        p_spec_r += normal_s[i]*p[i];
      p_spec_r += normal_s[dim];
    }
    return p_spec_r;
  }


  inline double value_at(const double * p) const
  {
    double sc = 0;
    for (int i = 0; i < dim; i++)
      sc += normal_s[i]*p[i];
    return sc + normal_s[dim];
  }

  int dim;
  int num_facet_incident_ridges;
  int num_facet_vertices;
  std::vector<int> outside_points;
  std::vector< std::list<Ridge>::iterator > ridges;//incident ridges
  
  double apex_dist;
  int apex;

  State state;

  Facet(int d): dim(d), num_facet_incident_ridges(d), num_facet_vertices(d)
  {
    normal_s = new double [dim+1];

    ridges.resize(num_facet_incident_ridges);
    apex_dist = -1;

    state = not_visited;
    p_spec = 0;
  }
  
  //special for hashtable lookup
  //dont forget to reset normal_s manually
  Facet(int d, double *normal_s_temp, size_t hash)
  {
    dim = d;
    normal_s = normal_s_temp;
    facet_hash = hash;
  }

  ~Facet()
  {
    delete [] normal_s;
  }
  bool operator<(const Facet& r)
  {
    return apex_dist > r.apex_dist;
  }

  void check_valid()
  {
    for (auto &r:ridges)
      r->check_valid();
  }

  bool is_outside(const int i, double * p)
  {
    double r = value_at(p);

    if (r < G_EPS_G)
      return 0;

    outside_points.push_back(i);

    if (apex_dist < r) {
      apex = i;
      apex_dist = r;
    }
    return 1;
  }
  inline void is_outside_ready(const int& i, const double& r)
  {
    outside_points.push_back(i);
    if (apex_dist < r) {
      apex = i;
      apex_dist = r;
    }
  }
  inline void set_apex(const int& i)
  {
    apex = i;
  }



  inline bool is_visible(double * p) const
  {
    return (value_at(p) > G_EPS_G);
  }

  void find_plane(const std::vector<int> & vertices, double * negative_side_point, double * pp)
  {
    assert(vertices.size() == dim);
    find_plane_s(vertices, negative_side_point, pp);
//    normal_norm = 1;
/*first create convertion from double* to Point to use this method
    plane = Plane(vertices.begin(), vertices.end(), negative_side_point, CGAL::ON_NEGATIVE_SIDE);
    Vector v = plane.orthogonal_vector();
    normal_norm = sqrt(v.squared_length());

    for (int i = 0; i < dim; i++)
      normal[i] = v[i]/normal_norm;
    f = plane.coefficient(dim)/normal_norm;
    */
  }

/*UNCOMMENT THIS FUNC IF YOU WANT FAST CALC*/
  void find_plane_s(const std::vector<int> & vertices, double * negative_side_point, double * pp)
  {//fast af
    assert(vertices.size() == dim);
    int n = dim;
    int m = n-1;
    //form n-1 vector in matrix (n-1)*n
    double A[m*n];
    double xl[n];
    for (int i = 0; i < n; i++)
      xl[i] = pp[vertices[n-1] + i];
    for (int i = 0; i < m; i++) {//row major
      for (int j = 0; j < n; j++)
        A[i*n + j] = pp[vertices[i] + j] - xl[j];
    }
    int max_row;
    for (int i = 0; i < m-1; i++) {
        double max = 0;
        for (int k = i; k < m; k++) {
            if (fabs(A[k*n+i]) > max) {
                max = fabs(A[k*n+i]);
                max_row = k;
            }
        }
#ifdef G_WITH_ADD_CHECKS_G
        if (max < 1e-10) {
          std::cout << "HELP!! " << std::endl;
        }
#endif
        if (i != max_row) {
          double b;
          for (int k = i; k < n; k++) {
            b = A[i*n+k];
            A[i*n+k] = A[max_row*n+k];
            A[max_row*n+k] = b;
          }
        }
        for (int j = i + 1; j < m; j++) {
            double coef = (A[j*n+i] / A[i*n+i]);
            for (int k = i + 1; k < n; k++)
                A[j*n+k] -= coef * A[i*n+k];
        }
    }
    normal_s[ m ] =  A[(m-1)*n + m-1];
    normal_s[m-1] = -A[(m-1)*n +  m ];
    for (int i = m - 2; i >= 0; i--) {//reverse
      double b = 0;
      for (int j = i+1; j < n; j++)
        b -= A[i*n + j]*normal_s[j];
      normal_s[i] = b/A[i*n + i];
    }
    normal_s[dim] = 0;
    for (int i = 0; i < n; i++)
      normal_s[dim] -= normal_s[i]*xl[i];

    double vv = 0;
    for (int i = 0; i < n; i++)
      vv += normal_s[i]*normal_s[i];
    vv = sqrt(vv);
  
    for (int i = 0; i < dim+1; i++)
      normal_s[i] = normal_s[i]/vv;
    if (value_at(negative_side_point) > 0) {
      for (int k = 0; k < dim+1; k++)
        normal_s[k] = -normal_s[k];
    }
    facet_hash = Point_hash(dim+1, normal_s);

  }


  friend std::ostream& operator<<(std::ostream& os, const Facet& f)
  {
    if (f.apex_dist > 0)
      os << "dist: " << f.apex_dist << " p: " << f.apex << std::endl;
    else
      os << "no max" << std::endl;
    //for (int i = 0; i < num_facet_incident_ridges; i++)
    //  os << "r" << i << " : " << *(f.ridges[i]) << std::endl;
    return os;
  }
};

void Ridge::from_invisible_to_not_visited()
{ //this is called only for boundary ridges
    //when they have only one invisible facet left
    assert(h0 == 1 && h1 == 0);
    r_facets[0]->state = not_visited;
}

void Ridge::check_valid()
{//call it when checking final convex hull
    assert(r_facets[0] != r_facets[1]);
    assert(h0 == 1 && h1 == 1 && is_broken_link == 0);
    assert(r_facets[0]->dim == dim && r_facets[1]->dim == dim);
    //not points to the unknown memory
}
struct Facet_it_hash
{
  size_t operator() (const std::list<Facet>::iterator & r) const //noexcept
  {
    return r->facet_hash;
  }
};
struct Facet_KeyEqual
{
  bool operator() (const std::list<Facet>::iterator &l,
                   const std::list<Facet>::iterator &r) const
  {
    for (int i = 0; i < l->dim+1; i++) {
      if (l->normal_s[i] != r->normal_s[i])
        return 0;
    }
    return 1;
  }
};
